package com.example.q.renderscriptexample.utils

import android.content.Context
import android.graphics.Bitmap
import android.support.v8.renderscript.Allocation
import android.support.v8.renderscript.Element
import android.support.v8.renderscript.RenderScript
import android.support.v8.renderscript.ScriptIntrinsicBlur
import android.support.v8.renderscript.Type

import com.example.q.renderscriptexample.ScriptC_histEq

/**
 * Created by q on 18/04/2016.
 */
object RenderScriptImageEdit {

    fun blurBitmap(bitmap: Bitmap, radius: Float, context: Context): Bitmap {
        //Create renderscript
        val rs = RenderScript.create(context)

        //Create allocation from Bitmap
        val allocation = Allocation.createFromBitmap(rs, bitmap)

        val t = allocation.type

        //Create allocation with the same type
        val blurredAllocation = Allocation.createTyped(rs, t)

        //Create script
        val blurScript = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        //Set blur radius (maximum 25.0)
        blurScript.setRadius(radius)
        //Set input for script
        blurScript.setInput(allocation)
        //Call script for output allocation
        blurScript.forEach(blurredAllocation)

        //Copy script result into bitmap
        blurredAllocation.copyTo(bitmap)

        //Destroy everything to free memory
        allocation.destroy()
        blurredAllocation.destroy()
        blurScript.destroy()
        t.destroy()

        return bitmap
    }

    fun histogramEqualization(image: Bitmap, context: Context): Bitmap {
        //Get image size
        val width = image.width
        val height = image.height

        //Create new bitmap
        val res = image.copy(image.config, true)

        //Create renderscript
        val rs = RenderScript.create(context)

        //Create allocation from Bitmap
        val allocationA = Allocation.createFromBitmap(rs, res)

        //Create allocation with same type
        val allocationB = Allocation.createTyped(rs, allocationA.type)

        //Create script from rs file.
        val histEqScript = ScriptC_histEq(rs)

        //Set size in script
        histEqScript._size = width * height

        //Call the first kernel.
        histEqScript.forEach_root(allocationA, allocationB)

        //Call the rs method to compute the remap array
        histEqScript.invoke_createRemapArray()

        //Call the second kernel
        histEqScript.forEach_remaptoRGB(allocationB, allocationA)

        //Copy script result into bitmap
        allocationA.copyTo(res)

        //Destroy everything to free memory
        allocationA.destroy()
        allocationB.destroy()
        histEqScript.destroy()
        rs.destroy()

        return res
    }

}//private constructor for utility class
