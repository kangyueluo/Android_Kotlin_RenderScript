package com.example.q.renderscriptexample

import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.provider.MediaStore
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.example.q.renderscriptexample.utils.RenderScriptImageEdit

import java.io.IOException

class EditPictureActivity : AppCompatActivity() {
    private var image: Bitmap? = null
    private var editedImage: Bitmap? = null

    private var mFloatingActionButton: FloatingActionButton? = null
    private var mTextView: TextView? = null
    private var mImageView: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_picture)

        if (intent.hasExtra(BITMAP_URI_EXTRA)) {
            val imageUri = Uri.parse(intent.getStringExtra(BITMAP_URI_EXTRA))
            try {
                image = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
            } catch (e: IOException) {
                e.printStackTrace()
                finish()
            }

        } else {
            finish()
        }
        val iv_original_image = findViewById(R.id.original_image) as ImageView?
        mImageView = findViewById(R.id.computed_image) as ImageView?
        mTextView = findViewById(R.id.time_textview) as TextView?
        mFloatingActionButton = findViewById(R.id.fab) as FloatingActionButton?
        iv_original_image!!.setImageBitmap(image)
        HistogramEqualizationTask().execute()

    }

    private fun setFAB(blur: Boolean) {
        if (blur) {
            mFloatingActionButton!!.visibility = View.GONE
        } else {
            mFloatingActionButton!!.setOnClickListener { BlurTask().execute() }
            mFloatingActionButton!!.visibility = View.VISIBLE
        }
    }

    private fun setEditedImageView(time: Long) {
        val text = getString(R.string.compute_time) + time + "ms for " + image!!.width + "x" + image!!.height + " pixels"
        mTextView!!.text = text
        mImageView!!.setImageBitmap(editedImage)
        setFAB(false)
    }

    private fun setBlurredImage(time: Long) {
        var text = mTextView!!.text.toString()
        text += " " + getString(R.string.blur_compute_time) + time + "ms"
        mTextView!!.text = text
        setFAB(true)
    }

    private inner class HistogramEqualizationTask : AsyncTask<Void, Void, Long>() {

        override fun doInBackground(vararg params: Void): Long? {
            val begin = System.currentTimeMillis()
            editedImage = RenderScriptImageEdit.histogramEqualization(image!!, this@EditPictureActivity)
            val end = System.currentTimeMillis()
            return end - begin
        }

        override fun onPostExecute(result: Long?) {
            setEditedImageView(result!!)
        }
    }

    private inner class BlurTask : AsyncTask<Void, Void, Long>() {

        override fun doInBackground(vararg params: Void): Long? {
            val begin = System.currentTimeMillis()
            editedImage = RenderScriptImageEdit.blurBitmap(editedImage!!, 25.0f, this@EditPictureActivity)
            val end = System.currentTimeMillis()
            return end - begin
        }

        override fun onPostExecute(result: Long?) {
            setBlurredImage(result!!)
        }
    }

    companion object {
        val BITMAP_URI_EXTRA = "BITMAP_URI_EXTRA"
    }
}
