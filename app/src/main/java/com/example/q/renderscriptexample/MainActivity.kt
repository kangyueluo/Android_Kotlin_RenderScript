package com.example.q.renderscriptexample

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        openPicker()
    }

    private fun openPicker() {
        val photoPickerIntent = Intent(Intent.ACTION_GET_CONTENT)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, PICKER_REQUEST_CODE)
    }

    private fun openEdit(uri: Uri) {
        val intent = Intent()
        intent.setClass(this@MainActivity, EditPictureActivity::class.java)
        intent.putExtra(EditPictureActivity.BITMAP_URI_EXTRA, uri.toString())
        startActivityForResult(intent, EDIT_RESULT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICKER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val chosenImageUri = data.data
                openEdit(chosenImageUri!!)
            } else {
                finish()
            }
        } else {
            openPicker()
        }
    }

    companion object {
        private val PICKER_REQUEST_CODE = 1
        private val EDIT_RESULT = 2
    }

}
